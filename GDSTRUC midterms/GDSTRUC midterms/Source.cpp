#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>


using namespace std;

int main()
{
	srand(time(NULL));
	cout << "ENTER SIZE OF ARRAY: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);

	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	cout << "\nGENERATED ARRAY: " << endl;

	cout << "UNORDERED: ";
	for (int i = 0; i < unordered.getSize(); i++)
	{
		cout << unordered[i] << " ";
	}

	cout << "\nORDERED: ";
	for (int i = 0; i < ordered.getSize(); i++)
	{
		cout << ordered[i] << " ";
	}

	system("pause");
	system("cls");

	while (true)
	{

		cout << "UNORDERED: ";
		for (int i = 0; i < unordered.getSize(); i++)
		{
			cout << unordered[i] << " ";
		}

		cout << "\nORDERED: ";
		for (int i = 0; i < ordered.getSize(); i++)
		{
			cout << ordered[i] << " ";
		}
		cout << endl;

		int choice = 0;
		cout << R"(PLEASE SELECT AN ACTION"
				1 - REMOVE AN ELEMENT
				2 - SEARCH AN ELEMENT
				3 - EXPAND AND ADD VALUES TO ARRAY 
				4 - EXIT)" << endl;
		cin >> choice;
		
		if (choice == 1)
		{
			int select;
			cout << "PLEASE REMOVE AN ELEMENT AT INDEX" << endl;
			cin >> select;

			unordered.remove(select);
			unordered.pop();

			ordered.remove(select);
			ordered.pop();

			system("pause");
			system("cls");
		}
		

		else if (choice == 2)
		{
			int count;
			cout << "\n\nENTER NUMBER TO SEARCH: ";
			int input;
			cin >> input;
			cout << endl;

			cout << "UNORDERED ARRAY(LINEAR SEARCH):\n";
			int result = unordered.linearSearch(input);

			if (result >= 0)
			{
				cout << input << " WAS FOUND AT INDEX " << result << ".\n";
			}

			else
			{
				cout << input << " IS NOT FOUND" << endl;
			}

			cout << "ORDERED ARRAY (BINARY SEARCH):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
			{
				cout << input << "  WAS FOUND AT INDEX " << result << ".\n";
			}
			else
			{
				cout << input << " NOT FOUND" << endl;
			}
			system("pause");
			system("cls");

		}

		else if (choice == 3)
		{
			int select;
			cout << "ENTER HOW MANY ELEMENTS WOULD YOU LIKE TO ADD" << endl;
			cin >> select;
			
			for (int i = 0; i < select; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
			system("pause");
			system("cls");
		}

		else if (choice == 4)
		{
			break;
		}

	}

	system("pause");
}